<?php
    include("plantilla_reporte.php");
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceReportes.php');
   
    //LLENADO DE DATOS
    $hora = date("h:i:s");
    $fecha = date("Y-m-j");
    $unitcli=$solescli=$unitigroup=$solesigroup=$difunit=$difsol=0.000;

    $service = new ServiceReportes();

    $data = $service->getReporteDiferenciaJerarquia();
    $registros = $data; 

    $porcentaje = $service->getPorcentajeAvance();

    $tamanoLetra = 7;

    $reportName = "REPORTE DE DIFERENCIAS POR JERARQUIA (".$porcentaje." %)";


    $borde = 0;
    $alineacion = "R";
    $altoFila = 4;

    $pdf = new PDF( 'P', 'mm', 'A4' );

    //foreach ($areas as $area) {


        $pdf->AddPage();

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 190, 5, $reportName, 0, 0, 'C' );
        $pdf->Ln(10);

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 5, $altoFila, 'N', $borde, 0, "L");
        $pdf->Cell( 70, $altoFila, 'JERARQUIAS', $borde, 0, "L");
        $pdf->Cell( 20, $altoFila, 'UNIT CLI', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'SOLES CLI', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'UNIT IGROUP', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'SOLES IGROUP', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'DIF UNIT', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'DIF SOL', $borde, 0, $alineacion);
        $pdf->Ln($altoFila);

        $i = 0;
        foreach ($registros as $fila) {

            $i++;
            $pdf->SetFont( 'Arial', '', $tamanoLetra );
            $pdf->Cell( 5, $altoFila, $i, $borde, 0, "L");
            $pdf->Cell( 70, $altoFila, $fila->linea, $borde, 0, "L");
            $pdf->Cell( 20, $altoFila, number_format($fila->stock,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($fila->stocksol,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($fila->contado,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($fila->contadosol,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($fila->difcant,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($fila->difsol,3), $borde, 0, $alineacion);
            $pdf->Ln($altoFila);

            $unitcli = $unitcli + $fila->stock;
            $solescli = $solescli + $fila->stocksol;
            $unitigroup = $unitigroup + $fila->contado;
            $solesigroup = $solesigroup + $fila->contadosol;
            $difunit = $difunit + $fila->difcant;
            $difsol = $difsol + $fila->difsol;

        }

            $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
            $pdf->Cell( 5, $altoFila, '', $borde, 0, "L");
            $pdf->Cell( 70, $altoFila, 'TOTALES', $borde, 0, "L");
            $pdf->Cell( 20, $altoFila, number_format($unitcli,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($solescli,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($unitigroup,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($solesigroup,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($difunit,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($difsol,3), $borde, 0, $alineacion);
            $pdf->Ln($altoFila);



  $pdf->Output( "reporte_diferencia_jerarquia.pdf", "I" );



?>