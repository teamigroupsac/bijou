<?php
    include("plantilla_reporte.php");
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceReportes.php');
   
    //LLENADO DE DATOS
    $hora = date("h:i:s");
    $fecha = date("Y-m-d");

    $service = new ServiceReportes();
    $estado = 1; //ACTIVO

    $data = $service->getReporteProductividad($estado);
    $registros = $data;


    $tamanoLetra = 7;

    $reportName = "REPORTE DE PRODUCTIVIDAD";


    $borde = 0;
    $alineacion = "L";
    $altoFila = 4;

    $pdf = new PDF( 'P', 'mm', 'A4' );

    //foreach ($areas as $area) {


        $pdf->AddPage();

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 190, 5, $reportName, 0, 0, 'C' );
        $pdf->Ln(10);

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 5, $altoFila, 'N', $borde, 0, $alineacion);
        $pdf->Cell( 15, $altoFila, 'DNI', $borde, 0, $alineacion);
        $pdf->Cell( 50, $altoFila, 'NOMBRES', $borde, 0, $alineacion);
        $pdf->Cell( 25, $altoFila, 'INICIO CONTEO', $borde, 0, $alineacion);
        $pdf->Cell( 25, $altoFila, 'FIN CONTEO', $borde, 0, $alineacion);
        $pdf->Cell( 25, $altoFila, 'HORAS CONTEO', $borde, 0, $alineacion);
        $pdf->Cell( 25, $altoFila, 'TOTAL CONTADO', $borde, 0, $alineacion);
        $pdf->Cell( 25, $altoFila, 'CONTEO X HORA', $borde, 0, $alineacion);
        $pdf->Ln($altoFila);

        $i = 0;
        $horas_conteo = 0.000;
        $total_conteo = 0.000;
        $conteo_x_hora = 0.000;
        foreach ($registros as $fila) {
            $i++;
            $pdf->SetFont( 'Arial', '', $tamanoLetra );
            $pdf->Cell( 5, $altoFila, $i, $borde, 0, $alineacion);
            $pdf->Cell( 15, $altoFila, $fila->dniUsuario, $borde, 0, $alineacion);
            $pdf->Cell( 50, $altoFila, $fila->nombreUsuario, $borde, 0, $alineacion);
            $pdf->Cell( 25, $altoFila, $fila->inicioAsistencia, $borde, 0, $alineacion);
            $pdf->Cell( 25, $altoFila, $fila->terminoAsistencia, $borde, 0, $alineacion);
            $pdf->Cell( 25, $altoFila, number_format($fila->horas_conteo,3), $borde, 0, 'C');
            $pdf->Cell( 25, $altoFila, number_format($fila->total_conteo,3), $borde, 0, 'C');
            $pdf->Cell( 25, $altoFila, number_format($fila->conteo_x_hora,3), $borde, 0, 'C');
            $pdf->Ln($altoFila);

            $horas_conteo = $horas_conteo + $fila->horas_conteo;
            $total_conteo = $total_conteo + $fila->total_conteo;
            $conteo_x_hora = $conteo_x_hora + $fila->conteo_x_hora;

        }
            $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
            $pdf->Cell( 5, $altoFila, "", $borde, 0, $alineacion);
            $pdf->Cell( 15, $altoFila, "", $borde, 0, $alineacion);
            $pdf->Cell( 50, $altoFila, "", $borde, 0, $alineacion);
            $pdf->Cell( 25, $altoFila, "TOTALES : ", $borde, 0, $alineacion);
            $pdf->Cell( 25, $altoFila, "", $borde, 0, $alineacion);
            $pdf->Cell( 25, $altoFila, number_format($horas_conteo,3), $borde, 0, 'C');
            $pdf->Cell( 25, $altoFila, number_format($total_conteo,3), $borde, 0, 'C');
            $pdf->Cell( 25, $altoFila, number_format($conteo_x_hora,3), $borde, 0, 'C');

    //}


  $pdf->Output( "reporte_usuario.pdf", "I" );



?>