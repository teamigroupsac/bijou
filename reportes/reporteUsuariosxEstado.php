<?php
    include("plantilla_reporte.php");
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceReportes.php');
   
    //LLENADO DE DATOS
    $hora = date("h:i:s");
    $fecha = date("Y-m-j");

    $service = new ServiceReportes();
    $estado = 1; //ACTIVO

    $data = $service->getListaUsuariosxEstado($estado);
    $registros = $data;


    $tamanoLetra = 8;

    $reportName = "REPORTE DE USUARIOS ACTIVOS";


    $borde = 0;
    $alineacion = "L";
    $altoFila = 4;

    $pdf = new PDF( 'P', 'mm', 'A4' );

    //foreach ($areas as $area) {


        $pdf->AddPage();

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 190, 5, $reportName, 0, 0, 'C' );
        $pdf->Ln(10);

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 5, $altoFila, 'N', $borde, 0, $alineacion);
        $pdf->Cell( 30, $altoFila, 'DNI', $borde, 0, $alineacion);
        $pdf->Cell( 65, $altoFila, 'COLABORADOR', $borde, 0, $alineacion);
        $pdf->Cell( 40, $altoFila, 'TIPO', $borde, 0, $alineacion);
        $pdf->Cell( 40, $altoFila, 'ESTADO', $borde, 0, $alineacion);
        $pdf->Ln($altoFila);

        $i = 0;
        foreach ($registros as $fila) {

            $i++;
            $pdf->SetFont( 'Arial', '', $tamanoLetra );
            $pdf->Cell( 5, $altoFila, $i, $borde, 0, $alineacion);
            $pdf->Cell( 30, $altoFila, $fila->dniUsuario, $borde, 0, $alineacion);
            $pdf->Cell( 65, $altoFila, $fila->nombreUsuario, $borde, 0, $alineacion);
            $pdf->Cell( 40, $altoFila, $fila->descripcionTipoUsuario, $borde, 0, $alineacion);
            $pdf->Cell( 40, $altoFila, $fila->descripcionEstadoUsuario, $borde, 0, $alineacion);
            $pdf->Ln($altoFila);

        }

    //}


  $pdf->Output( "reporte_usuario.pdf", "I" );



?>