var service = new Service("webService/index.php");


$(function() 
{

iniciarArchivosPendientes();

});

var arrayUsuarios = [];

function iniciarArchivosPendientes(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");


    $("#menu_archivos_pendientes").on('click', function(){
      cargaConsultasIniciales();  
    })

    $("#boton_menu_archivos_pendientes").on('click', function(){
      cargaConsultasIniciales();  
    })

    function cargaConsultasIniciales(){
        //service.procesar("listarArchivosPendientes",cargaListarArchivosPendientes);
        //service.procesar("getListaEstados",cargaListaEstados);
        //service.procesar("getListaTipos",cargaListaTipos);
        //cargarPeriodos();
        cargarListaArchivosPendientesTodos();
    }

    function cargarListaArchivosPendientesTodos(){
        service.procesar("listarArchivosPendientes",cargaListarArchivosPendientesTodos);
    }

    $("#listarArchivosPendientes").on('click',function(){
        service.procesar("listarArchivosPendientes",cargaListarArchivosPendientes);
    })

    function cargaListarArchivosPendientes(evt){
        resultado = evt;

        $("#tablaListaArchivosPendientes tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            //(i + 1)
            fila.html('<td>'+(i+1)+'</td><td>'+ resultado[i].nombre +'</td><td>'+ resultado[i].fecha +'</td><td>'+ resultado[i].peso +'</td>'); //<td>'+ resultado[i].descripcion +'</td>

            var contenedorBotones = $("<td>");

            var btnremover = $("<button class='btn btn-danger btn-sm' title='Remover'>");
            var btnCargar = $("<button class='btn btn-success btn-sm' title='Cargar'>");

            btnremover.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            btnCargar.html('<span class="glyphicon glyphicon-download-alt"></span> CARGAR');

            btnremover.data("data",datoRegistro);
            btnCargar.data("data",datoRegistro);

            btnremover.on("click",removerArchivosPendientes);
            btnCargar.on("click",cargarArchivosPendientes);

            contenedorBotones.append(btnremover);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnCargar);

            fila.append(contenedorBotones);

            $("#tablaListaArchivosPendientes tbody").append(fila);

            service.procesar("cargarArchivoPendienteMasivo",datoRegistro.nombre,mensajeCargarArchivoPendienteMasivo);

           
            if(parseFloat(i) == (parseFloat(resultado.length)-1)){
               
                backupBaseDatos();                
            }

        }

        service.procesar("listarArchivosPendientes",cargaListarArchivosPendientesMasivo);

    }


    function cargaListarArchivosPendientesTodos(evt){
        resultado = evt;

        $("#tablaListaArchivosPendientes tbody").html("");

        //LLENADO DE INFORMACION EN EL AREA DE AVANCE
        //service.procesar("getListaAreaRango",resultadoAvanceCapturaInicial);

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            //(i + 1)
            fila.html('<td>'+(i+1)+'</td><td>'+ resultado[i].nombre +'</td><td>'+ resultado[i].fecha +'</td><td>'+ resultado[i].peso +'</td>'); //<td>'+ resultado[i].descripcion +'</td>

            var contenedorBotones = $("<td>");

            var btnremover = $("<button class='btn btn-danger btn-sm' title='Remover'>");
            var btnCargar = $("<button class='btn btn-success btn-sm' title='Cargar'>");

            btnremover.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            btnCargar.html('<span class="glyphicon glyphicon-download-alt"></span> CARGAR');

            btnremover.data("data",datoRegistro);
            btnCargar.data("data",datoRegistro);

            btnremover.on("click",removerArchivosPendientes);
            btnCargar.on("click",cargarArchivosPendientes);

            contenedorBotones.append(btnremover);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnCargar);

            fila.append(contenedorBotones);

            $("#tablaListaArchivosPendientes tbody").append(fila);

        }

    }


    function mensajeCargarArchivoPendienteMasivo(evt){
        resultado = evt.estado;
        nombre = evt.nombre;

        if(resultado == 0){
            alertify.success("ARCHIVO CARGADO SATISFACTORIAMENTE");
            
        }else{
            alertify.error("ARCHIVO NO CARGADO, INTENTARLO NUEVAMENTE");
        }

    }

    function cargaListarArchivosPendientesMasivo(evt){
        resultado = evt;
        console.log(evt.length);

        if(evt.length > 0){
            $("#listarArchivosPendientes").show();
            $("#listarArchivosPendientes").html("( "+evt.length+" ) ARCHIVOS PENDIENTES DE CARGA");
        }else{
            $("#listarArchivosPendientes").hide();
        }


        $("#tablaListaArchivosPendientes tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            //(i + 1)
            fila.html('<td>'+(i+1)+'</td><td>'+ resultado[i].nombre +'</td><td>'+ resultado[i].fecha +'</td><td>'+ resultado[i].peso +'</td>'); //<td>'+ resultado[i].descripcion +'</td>

            var contenedorBotones = $("<td>");

            var btnremover = $("<button class='btn btn-danger btn-sm' title='Remover'>");
            var btnCargar = $("<button class='btn btn-success btn-sm' title='Cargar'>");

            btnremover.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            btnCargar.html('<span class="glyphicon glyphicon-download-alt"></span> CARGAR');

            btnremover.data("data",datoRegistro);
            btnCargar.data("data",datoRegistro);

            btnremover.on("click",removerArchivosPendientes);
            btnCargar.on("click",cargarArchivosPendientes);

            contenedorBotones.append(btnremover);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnCargar);

            fila.append(contenedorBotones);

            $("#tablaListaArchivosPendientes tbody").append(fila);

        }

    }

    function removerArchivosPendientes(){
        var data = $(this).data("data");
        nombre = data.nombre;
        alertify.confirm("¿ SEGURO DE ELIMINAR EL ARCHIVO : " + nombre +" ?", function (e) {
            if (e) {
                service.procesar("eliminarArchivoPendiente",nombre,mensajeCopiarArchivoPendiente);
            }
        });
       
    }

    function cargarArchivosPendientes(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE CARGAR EL ARCHIVO : " + data.nombre +" ?", function (e) {
            if (e) {
                service.procesar("cargarArchivoPendiente",data.nombre,mensajeCargarArchivoPendiente);
            }
        });

    }

    function mensajeCargarArchivoPendiente(evt){
        resultado = evt.estado;
        nombre = evt.nombre;

        if(resultado == 0){
            alertify.success("ARCHIVO CARGADO SATISFACTORIAMENTE");
            service.procesar("copiarArchivoPendiente",nombre,mensajeCopiarArchivoPendiente);
            backupBaseDatos();
        }else{
            alertify.error("ARCHIVO NO CARGADO, INTENTARLO NUEVAMENTE");
        }
    }

    function mensajeCopiarArchivoPendiente(evt){
        if(evt > 0){
            $("#listarArchivosPendientes").show();
            $("#listarArchivosPendientes").html("( "+evt+" ) ARCHIVOS PENDIENTES DE CARGA");
            service.procesar("listarArchivosPendientes",cargaListarArchivosPendientesTodos);
        }else{
            $("#listarArchivosPendientes").hide();
            service.procesar("listarArchivosPendientes",cargaListarArchivosPendientesTodos);
        }

    }



    //LLENADO DE INFORMACION EN EL AREA DE AVANCE

    function resultadoAvanceCapturaInicial(evt){
        listadeAreaRango = corregirNullArreglo(evt.rangos);
        listaCapturas = corregirNullArreglo(evt.capturas);
        listaJustificados = corregirNullArreglo(evt.justificados);

        var cantidad = 0;
        var avance = 0;
        var porcentaje = 0;

        for(var i=0; i<listadeAreaRango.length ; i++){
            var filaInicio =parseFloat(listadeAreaRango[i].area_ini_ran);
            var filaFinal = parseFloat(listadeAreaRango[i].area_fin_ran);

            for (var z=filaInicio; z<=filaFinal; z++) {
                cantidad++;
                for (var y=0; y<listaCapturas.length; y++) {
                    var valorArea = parseFloat(listaCapturas[y].area_cap);
                    if (z == valorArea){
                        avance++;
                    }
                }
                for (var y=0; y<listaJustificados.length; y++) {
                    var valorArea = parseFloat(listaJustificados[y].lote);
                    if (z == valorArea){
                        avance++;
                    }
                }
            }

        }

        porcentaje = ((parseFloat(avance) / parseFloat(cantidad))*100).toFixed(2);

        $(".progressAvanceCaptura").css('width', porcentaje+'%').attr('aria-valuenow', porcentaje);
        $(".progressAvanceCaptura").html(porcentaje+' %');      
    }

    function backupBaseDatos(){
        service.procesar("comprobarDispositivoPendientes",function(evt){
            if(evt == 1){

                openModalGenerarArchivo("BACKUP TABLAS DE BASE DATOS");
                service.procesar("backupBaseDatosCaptura",function(evt){
                    alertify.success("BACKUP REALIZADA SATISFACTORIAMENTE");
                    closeModalGenerarArchivo();
                });

            }else{

                alertify.error("DISPOSITIVO EN UNIDAD USB NO ENCONTRADO");

                alertify.confirm("¿ GUARDAR BACKUP EN UNIDAD LOCAL ?", function (e) {
                    if (e) {

                        openModalGenerarArchivo("BACKUP TABLAS DE BASE DATOS");
                        service.procesar("backupBaseDatosCaptura",function(evt){
                            alertify.success("BACKUP REALIZADA SATISFACTORIAMENTE");
                            closeModalGenerarArchivo();
                        });

                    } else {
                        alertify.error("HA CANCELADO EL PROCESO - REVISARA NUEVAMENTE LA UNIDAD USB");
                        backupBaseDatos();
                    }

                });

            }

        });


    }

    function openModalGenerarArchivo(archivo){
        $("#modal_cerrar_inventario_progress").modal('show');
        $("#modal_cerrar_inventario_progress .descripcionarchivogenerado").html("GENERANDO ARCHIVO "+ archivo );
    }

    function closeModalGenerarArchivo(){
        $("#modal_cerrar_inventario_progress").modal('hide');
        alertify.success("ARCHIVO GENERADO SATISFACTORIAMENTE");
    }







    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

    function corregirNullArreglo(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = "";
        }
        return resultado;
    }

}


