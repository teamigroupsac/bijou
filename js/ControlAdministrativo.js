var service = new Service("webService/index.php");

var cuestionario = Array;


$(function() 
{

iniciarControlAdministrativo();

});


function iniciarControlAdministrativo(){

	var dniUsuario = sessionStorage.getItem("dniUsuario");
	var nombreUsuario = sessionStorage.getItem("nombreUsuario");
	var tipoUsuario = sessionStorage.getItem("tipoUsuario");
	var codigo_usuario = sessionStorage.getItem("logeado");


	$("#usuario").html(nombreUsuario);
	$("#tipo").html(tipoUsuario);


	//LLENADO DE INFORMACION EN EL AREA DE AVANCE
    //service.procesar("archivosDescargados",resultadoArchivosDescargados);
	service.procesar("getAvanceCaptura",resultadoAvanceCapturaInicial);


	function resultadoAvanceCapturaInicial(evt){

        porcentaje = evt;
        porcentaje = corregirNull(porcentaje);

		$(".progressAvanceCaptura").css('width', porcentaje+'%').attr('aria-valuenow', porcentaje);
		$(".progressAvanceCaptura").html(porcentaje+' %'); 		
	}


    $("#listarConflictoBarras").on('click',function(){
        service.procesar("listarConflictoBarras",resultadoListarConflictoBarras);
    })

    function resultadoListarConflictoBarras(evt){
        resultado = evt;
        $("#tablaConflictoBarras tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){

            var fila = $("<tr>");
            fila.html('<td>'+ (i + 1) +'</td><td>'+ resultado[i].area_cap +'</td><td>'+ resultado[i].barra_cap +'</td><td>'+ resultado[i].cant_cap +'</td>');
            $("#tablaConflictoBarras tbody").append(fila);

        }
    }

    function resultadoArchivosDescargados(evt){
        //console.log(evt);
        $("#principal .total").html(evt.total);
        $("#principal .pendientes").html(evt.pendientes);
        $("#principal .procesados").html(evt.procesados);

    }



    function corregirNull(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = 0;
        }
        return resultado;
    }

    function corregirNullArreglo(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = [];
        }
        return resultado;
    }

}


