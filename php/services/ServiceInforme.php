<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceInforme extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function getCuestionarioAlmacenBodega(){
		$sql = "SELECT idCuestionario, cuestionario FROM reportecuestionario
				WHERE idReporte = 1
				ORDER BY idCuestionario ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("cuestionario"));

		$resultado = new stdClass();
        $resultado->cuestionario = $res;
        $resultado->numeroTienda = $this->getDato("numeroTienda","tienda","idTienda > 0 ORDER BY idTienda DESC LIMIT 1");
        $resultado->nombreTienda = $this->getDato("nombreTienda","tienda","idTienda > 0 ORDER BY idTienda DESC LIMIT 1");

		return $resultado;
	}

	function getCuestionarioPisoVenta(){
		$sql = "SELECT idCuestionario, cuestionario FROM reportecuestionario
				WHERE idReporte = 2
				ORDER BY idCuestionario ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("cuestionario"));
		return $res;
	}

	function getPreguntasInforme($cuestionario){
		$sql = "SELECT RP.idPregunta, RP.pregunta, RC.idReporte FROM reportepregunta RP LEFT JOIN reportecuestionario RC
				ON RP.idCuestionario = RC.idCuestionario
				WHERE RP.idCuestionario = $cuestionario
				ORDER BY RP.orden ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("pregunta"));

		$resultado = new stdClass();
        $resultado->cuestionario = $cuestionario;
        $resultado->preguntas = $res;

		return $resultado;
	}

	function getRespuestasInforme($cuestionario,$pregunta){
		$sql = "SELECT RR.idRespuesta, RR.respuesta, IF(RRP.idRespuesta = RR.idRespuesta,'SELECTED','') seleccionado FROM reportepregunta RP LEFT JOIN reporterespuesta RR
				ON RP.idGrupo = RR.idGrupo LEFT JOIN respuestaspreguntas RRP
				ON RP.idPregunta = RRP.idPregunta
				WHERE RP.idPregunta = $pregunta
				ORDER BY RR.orden ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("respuesta"));

		$resultado = new stdClass();
		$resultado->cuestionario = $cuestionario;
        $resultado->pregunta = $pregunta;
        $resultado->respuestas = $res;

		return $resultado;
	}

	function saveInformeAlmacenBodega($data){
		$sql = "UPDATE respuestasinforme SET 
				fecha = '$data->fecha',
				responsableIgroup = UPPER('$data->responsableIgroup'),
				responsableCliente = UPPER('$data->responsableCliente'),
				encargadoCliente = UPPER('$data->encargadoCliente'),
				inicioConteo = '$data->inicioConteo',
				supervisorIgroup = UPPER('$data->supervisorIgroup'),
				finConteo = '$data->finConteo',
				observaciones = '$data->observaciones'
				WHERE idInforme = 1";
		$res=$this->db->query($sql);
		return "OK";
	}

	function saveInformePisoVenta($data){
		$sql = "UPDATE respuestasinforme SET 
				encargadoCliente = UPPER('$data->encargadoCliente'),
				inicioConteo = '$data->inicioConteo',
				supervisorIgroup = UPPER('$data->supervisorIgroup'),
				finConteo = '$data->finConteo',
				mesaControl = UPPER('$data->mesaControl'),
				cierreInventario = '$data->cierreInventario',
				observaciones = '$data->observaciones'
				WHERE idInforme = 2";
		$res=$this->db->query($sql);
		return "OK";
	}

	function savePreguntasInforme($idPregunta,$idRespuesta){
        $sql="DELETE FROM respuestaspreguntas WHERE idPregunta= $idPregunta";
        $res=$this->db->query($sql);

        $sql="INSERT INTO respuestaspreguntas (idPregunta,idRespuesta) VALUES ($idPregunta,$idRespuesta)";
		$res=$this->db->query($sql);
	}




	function getDatosCuestionarioAB($codigo){
		$sql = "SELECT * FROM respuestasinforme WHERE idInforme = $codigo";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("responsableIgroup","responsableCliente","encargadoCliente","supervisorIgroup","observaciones"));
		return $res;
	}

	function getDatosCuestionarioPV($codigo){
		$sql = "SELECT * FROM respuestasinforme WHERE idInforme = $codigo";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("encargadoCliente","supervisorIgroup","mesaControl","observaciones"));
		return $res;
	}

	function getPreguntas($cuestionario){
		if($cuestionario != ""){
			$filtroCuestionario = " AND RC.idCuestionario = ".$cuestionario;
		}
		$sql = "SELECT RP.idPregunta, RP.pregunta, RP.orden, RC.idCuestionario, RC.cuestionario, RG.idGrupo, RG.grupo FROM reportepregunta RP LEFT JOIN reportecuestionario RC
				ON RP.idCuestionario = RC.idCuestionario LEFT JOIN reportegrupo RG
				ON RP.idGrupo = RG.idGrupo
				WHERE RP.idPregunta > 0 $filtroCuestionario
				ORDER BY RC.idCuestionario, RP.orden ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("pregunta","cuestionario","grupo"));
		return $res;
	}

	function getCuestionarios(){
		$sql = "SELECT * FROM reportecuestionario";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("cuestionario"));
		return $res;
	}

	function getGrupos(){
		$sql = "SELECT * FROM reportegrupo";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("grupo"));
		return $res;
	}




	function saveFormularioPregunta($data){
		$procedimiento = $data->procedimiento;
		$idPregunta = $data->idPregunta;
		$nombrePregunta = $data->nombrePregunta;
		$ordenPregunta = $data->ordenPregunta;
		$cuestionarioPregunta = $data->cuestionarioPregunta;
		$grupoPregunta = $data->grupoPregunta;
		$usuario = $data->usuario;

		if($procedimiento == "GUARDAR"){
			$sql="INSERT INTO reportepregunta (pregunta,idCuestionario,orden,idGrupo)
				VALUES (UPPER('$nombrePregunta'),$cuestionarioPregunta,$ordenPregunta,$grupoPregunta)";

			$resNuevo=$this->db->query($sql);
		}else if($procedimiento == "MODIFICAR"){
			$sql="UPDATE reportepregunta SET 
				pregunta = UPPER('$nombrePregunta'),
				idCuestionario = $cuestionarioPregunta,
				orden = $ordenPregunta,
				idGrupo = $grupoPregunta
				WHERE idPregunta = $idPregunta";

			$resEditado=$this->db->query($sql);
		}

		if($resNuevo){
			return 1;
		}elseif($resEditado){
			return 2;
		}else{
			return 0;
		}

	}

    function deleteFormularioPregunta($dato){
        $sql="DELETE FROM reportepregunta WHERE idPregunta= $dato";
        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }

    function eliminarMasivoFormularioPregunta($data){
    	$usuarioRegistrador = $data->usuario;
    	$preguntas = $data->preguntas;

    	$listapregunta = implode(",", $preguntas);

        $sql="DELETE FROM reportepregunta WHERE idPregunta IN ($listapregunta)";

        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }




}	
?>