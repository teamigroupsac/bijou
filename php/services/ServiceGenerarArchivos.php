
<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceGenerarArchivos extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function generarArchivoUsuario($dato){
		$condicion = "";
		if($dato != ""){
			$condicion = "WHERE estadoUsuario = $dato";
		}

		$sql = "SELECT dniUsuario,claveUsuario,tipoUsuario FROM usuario $condicion and tipoUsuario<>4";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/usuarios.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;

		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->dniUsuario."|".$res[$i]->claveUsuario."|".$res[$i]->tipoUsuario;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;

	}

	function generarArchivoMaestro(){

		$sql = "SELECT DISTINCT cod_barra FROM maestro UNION DISTINCT SELECT DISTINCT sku FROM maestro";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/maestro.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;

		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->cod_barra;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;

	}

	function generarArchivoMaestroDetalle(){

		$sql = "SELECT cod_barra, des_barra FROM maestro GROUP BY cod_barra";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/maestro_detalle.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;

		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->cod_barra."|".$res[$i]->des_barra;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;

	}


	function comprobarInformacionArchivo($file){
		$archivo = "../archivos_sistema/archivos_generados/".$file.".txt";
		$bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );
    	$conteo = count(file($archivo));

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = ($conteo - 1);
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}


	function generarprimerarchivo(){
		$sql="	SELECT CAST(M.nro_cont_mst AS UNSIGNED) nro_cont_mst,CAST(S.nro_cont_stk AS UNSIGNED) nro_cont_stk,M.loc_mst,S.loc_stk,
		RIGHT(CONCAT('00000000',C.sku_cap),8) sku_cap,
		RIGHT(CONCAT('0000000000000',C.barra_cap),13) barra_cap,
		RIGHT(CONCAT('00',C.tip_cap),2) tip_cap,
		C.cant_cap,
		CONCAT(SUBSTRING(M.fec_cong_mst, 7,4),SUBSTRING(M.fec_cong_mst, 4,2),SUBSTRING(M.fec_cong_mst, 1,2)) fec_cong_mst,
		CONCAT(SUBSTRING(S.fec_cong_stk, 7,4),SUBSTRING(S.fec_cong_stk, 4,2),SUBSTRING(S.fec_cong_stk, 1,2)) fec_cong_stk
				FROM maestro M INNER JOIN capturas C
				ON M.sku_barra = C.sku_cap LEFT JOIN stock S
				ON M.sku_barra = S.sku_stk
				group by c.sku_cap ";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/IF_CARGA.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;

		for($i=0;$i<count($res);$i++){
			$cant_cap = $res[$i]->cant_cap;
			if($cant_cap < 0){
				$n_cant_cap = "-".SUBSTR("00000000000000000".($cant_cap * (-1)),-17);
			}else{
				$n_cant_cap = SUBSTR("000000000000000000".$cant_cap,-18);
			}
			$conteo++;

			//SI EL VALOR DE STOCK TIENE DATO ENTONCES COLOCAR EL VALOR SINO DEJAR EL VALOR DE MAESTRO
			$cadena.="\r\n";
			$cadena.= $res[$i]->nro_cont_stk."|".$res[$i]->loc_stk."|".$res[$i]->sku_cap."|".$res[$i]->barra_cap."|".$res[$i]->tip_cap."|".$n_cant_cap."|".$res[$i]->fec_cong_stk;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}

	function generarsegundoarchivo(){
		$sql="	SELECT c.area_cap, c.barra_cap, c.sku_cap,c.des_barra, c.precio, c.linea, 
				IF(sum(c.cant_cap)<>0,sum(c.cant_cap),0.000) as BODEGA, 
				'0.000' as PISO_VENTA 
				FROM capturas c, area_rango r where c.area_cap between r.area_ini_ran and r.area_fin_ran AND r.ubicacion='B'
				group by c.sku_cap, c.area_cap, r.ubicacion
				UNION DISTINCT
				SELECT c.area_cap, c.barra_cap, c.sku_cap, c.des_barra, c.precio, c.linea, '0.000' as BODEGA,  
				IF(sum(c.cant_cap)<>0,sum(c.cant_cap),0.000) as PISO_VENTA
				FROM capturas c,area_rango r where c.area_cap between r.area_ini_ran and r.area_fin_ran AND r.ubicacion='P'
				group by c.sku_cap, c.area_cap, r.ubicacion order by sku_cap, area_cap ";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/BOD_SV.txt";
		unlink('$archivo');

		$cadena="\r\n";
		$conteo=1;

		$cadena.="LOTE|SKU|EAN|DESCRIPCION|COSTO|JERARQUIA|BODEGA|SALA";
		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->area_cap."|".$res[$i]->sku_cap."|".$res[$i]->barra_cap."|".$res[$i]->des_barra."|".$res[$i]->precio."|".$res[$i]->linea."|".$res[$i]->BODEGA."|".$res[$i]->PISO_VENTA;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}














	function generarreporteconteoarchivo(){
		$sql="	SELECT area_cap,sku_cap,barra_cap,des_barra,SUM(cant_cap) cantidad FROM CAPTURAS
				GROUP BY area_cap,sku_cap,barra_cap";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/REPORTE_CONTEO.txt";
		unlink('$archivo');

		$cadena="\r\n";
		$conteo=1;

		$cadena.="AREA|SKU|EAN|DESCRIPCION|CANTIDAD";
		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->area_cap."|".$res[$i]->sku_cap."|".$res[$i]->barra_cap."|".$res[$i]->des_barra."|".$res[$i]->cantidad;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}
	function floattostr($val)
	{
	    preg_match( "#^([\+\-]|)([0-9]*)(\.([0-9]*?)|)(0*)$#", trim($val), $o );
	    return $o[1].sprintf('%d',$o[2]).($o[3]!='.'?$o[3]:'');
	}

	function generarbalarchivo(){
		
		$sql="	SELECT c.sku_cap, lpad(round(sum(CASE WHEN c.area_cap between ar.area_ini_ran and ar.area_fin_ran and ar.des_area_ran='RESERVA' THEN c.cant_cap ELSE 0 END)),6,' ') AS reserva,
				lpad(round(sum(CASE WHEN c.area_cap between ar.area_ini_ran and ar.area_fin_ran and ar.des_area_ran='PINCHE' THEN c.cant_cap ELSE 0 END)),7,' ') AS pinche,
				lpad(round(sum(CASE WHEN c.area_cap between ar.area_ini_ran and ar.area_fin_ran and ar.des_area_ran='DEVOL' THEN c.cant_cap ELSE 0 END)),7,' ') AS devol,
				lpad(round(sum(CASE WHEN c.area_cap between ar.area_ini_ran and ar.area_fin_ran and ar.des_area_ran='FALLADO' THEN c.cant_cap ELSE 0 END)),7,' ') AS fallado,
				lpad(round(sum(CASE WHEN c.area_cap between ar.area_ini_ran and ar.area_fin_ran and ar.des_area_ran='S/CON' THEN c.cant_cap ELSE 0 END)),7,' ') AS 'scon',
				LPAD(ROUND(SUM(round(c.cant_cap))),8,' ') as total, ROUND(m.precio * sum(c.cant_cap),1) AS importe
				FROM captura c, area_rango ar, maestro m
				WHERE c.sku_cap=m.sku AND c.area_cap BETWEEN ar.area_ini_ran AND ar.area_fin_ran
				GROUP BY c.sku_cap ";
		$res = $this->db->get_results($sql);

		$sqlTienda = "SELECT * FROM tienda ORDER BY idTienda ASC LIMIT 1";
		$resTienda = $this->db->get_results($sqlTienda);

		$sucursal = $resTienda[0]->numeroTienda;
		$fecha = date("d_m_Y");

		$archivo = "../archivos_sistema/archivos_generados/".$sucursal."_".$fecha.".BAL";
		unlink('$archivo');

		$cadena='"'.'"Sucursal: '. $sucursal.' - Fecha: '.date("d/m/Y").'  - Fecha de Creacion: '.date("d/m/Y H:i:s A").'"'."\r\n";
		$cadena.='"------------------------------------------------------------"'."\r\n";
		$cadena.='"CODIGO  RESERVA  PINCHE  DEVOL FALLADO S/CON  TOTAL  IMPORTE"'."\r\n";
		$cadena.='"------------------------------------------------------------"'."\r\n";
		$j=0;
		$k=0;
		$l=0;
		$m=0;
		$n=0;
		$o=0;
		$p=0;
		
		//while($fila = mysql_fetch_array($sql)){

		for ($i=0; $i < count($res); $i++) { 
			$importe = 0;
			$j=$j+$res[$i]->reserva;
			$k=$k+$res[$i]->pinche;
			$l=$l+$res[$i]->devol;
			$m=$m+$res[$i]->fallado;
			$n=$n+$res[$i]->scon;
			$o=$o+$res[$i]->total;
			$p=$p+$res[$i]->importe;

			$decimales = explode(".",$res[$i]->importe);
			if (($res[$i]->importe - $decimales[0]) > 0) { 
				$importe = SUBSTR("          ".$res[$i]->importe,-10);
			}else{
				$importe = SUBSTR("          ".$decimales[0],-10);
			}

			//for($x=0; $x < count($res); $x++){
				//if($i==0){
			$cadena.= '"'.$res[$i]->sku_cap.$res[$i]->reserva.$res[$i]->pinche.$res[$i]->devol.$res[$i]->fallado.$res[$i]->scon.$res[$i]->total.$importe;
				//}else{
				//	$cadena.= "".$res[$i];
				//}	
			//}
			
			$cadena.= '"'."\r\n";

		}
		
		$cadena.='--------------------------------------------------------------'."\r\n";
		$cadena.='TOTAL        '.$j."    ".$k."      ".$l."     ".$m."     ".$n."    ".$o."     ".$p.""."\r\n";
		$cadena.='--------------------------------------------------------------';
		/*
		$cadena="\r\n";
		$conteo=1;

		$cadena.="LOTE|SKU|DESCRIPCION|EAN|COSTO|STOCK|JERARQUIA|BODEGA|SALA";
		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->area_cap."|".$res[$i]->sku_cap."|".$res[$i]->barra_cap."|".$res[$i]->precio_barra."|".$res[$i]->jerar."|".$res[$i]->BODEGA."|".$res[$i]->PISO_VENTA;
        }
		*/
	
		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        //$file->nombre_archivo = $tienda."-".$fecha.".BAL";
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}

	function getTiendaInventario(){
		$sql = "SELECT * FROM tienda ORDER BY idTienda ASC LIMIT 1";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("nombreTienda"));
		return $res;
	}
/*
	function generarReporteProductividad($estado){
		$sql="	SELECT U.dniUsuario, U.nombreUsuario,
				(SELECT inicioAsistencia FROM asistencia WHERE dniUsuario = U.dniUsuario ORDER BY inicioAsistencia ASC LIMIT 1) inicioAsistencia, 
				(SELECT terminoAsistencia FROM asistencia WHERE dniUsuario = U.dniUsuario ORDER BY inicioAsistencia DESC LIMIT 1) terminoAsistencia,
				SUM((TIMESTAMPDIFF(SECOND , A.inicioAsistencia, A.terminoAsistencia ))/60/60) horas_conteo,
				(SELECT SUM(cant_cap) FROM captura WHERE usuario = U.dniUsuario) total_conteo,
				((SELECT SUM(cant_cap) FROM captura WHERE usuario = U.dniUsuario) / SUM((TIMESTAMPDIFF(SECOND , A.inicioAsistencia, A.terminoAsistencia ))/60/60)) conteo_x_hora 
				FROM usuario U LEFT JOIN asistencia A
				ON U.dniUsuario = A.dniUsuario
				WHERE U.estadoUsuario = $estado
				GROUP BY U.dniUsuario";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/archivo_reporte_productividad.txt";
		unlink('$archivo');

		$conteo=1;
		$cadena.="\r\n";
		$cadena.="N|DNI|NOMBRE COMPLETO|INICIO CONTEO|FIN CONTEO|HORAS CONTEO|TOTAL CONTADO|CONTEO X HORA";
		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $conteo."|".$res[$i]->dniUsuario."|".$res[$i]->nombreUsuario."|".$res[$i]->inicioAsistencia."|".$res[$i]->terminoAsistencia."|".$res[$i]->horas_conteo."|".$res[$i]->total_conteo."|".$res[$i]->conteo_x_hora;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}
*/

}	
?>