<div class="modal fade" tabindex="-1" role="dialog" id="modalFirmaAlmacenBodega">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title firmaModalAlmacenBodega"></h4>
            </div>
            <div class="modal-body">
                
                <div class="row">
                    <div class="col-md-4">
                        <button id="bt-clear" class="btn btn-danger btn-sm btn-block">LIMPIAR</button>
                    </div>
                    <div class="col-md-4">
                        <button id="bt-save" class="btn btn-success btn-sm btn-block">GUARDAR</button>
                    </div>
                    <div class="col-md-4">
                        <input type="color" id="color" class="form-control input-sm" value="#000000">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="canvas" width="853" height="400" style="top:10%;left:10%;border:2px solid;"></canvas>
                    </div>
                </div>
                      
                   

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->